#
# LaTeX Makefile - Martin Pieuchot
#   http://nolizard.org
#

SRCS= en.tex fr.tex es.tex pt.tex
INCS= curriculum.tex
PDFS= $(SRCS:.tex=.pdf)

PTEX?= 	pdflatex
PFLAGS=	-halt-on-error -file-line-error

SUBST_CMD= perl -p -i -e

.SUFFIXES: .tex .pdf

.tex.pdf: $(INCS)
	$(PTEX) $(PFLAGS) $?

all: $(PDFS)

clean: unmail
	-rm -f $(PDFS) *.log *.out *.aux

.PHONY: all clean

#
# I don't write my email on the CV I put on internet
mail: $(SRCS)
	$(SUBST_CMD) 's/(mpieuchot@)/\1nolizard.org/' $?

unmail: $(SRCS)
	$(SUBST_CMD) 's/\@nolizard\.org/@/' $?

